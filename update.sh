#!/bin/bash

cd public/
dpkg-scanpackages . /dev/null > Release
dpkg-scanpackages . /dev/null | gzip -9c > Packages.gz

cd ../
git add .
git commit -m "Updated packages"
git push
